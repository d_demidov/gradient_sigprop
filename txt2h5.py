#!/usr/bin/python

import sys, h5py
from numpy import *

if len(sys.argv) < 8:
    print("Usage: %s Sxx.txt ... Syz.txt Z.txt" % sys.argv[0])
    sys.exit(1)

def read_signal(fname):
    f = open(fname, 'r')

    nsen, lmod = fromfile(f, dtype=int32, count=2, sep=' ')
    data = fromfile(f, dtype=float64, count=3 * nsen * lmod, sep=' ')
    f.close()

    data.shape = (nsen, lmod, 3)

    return swapaxes(data, 1, 2)

S = h5py.File("S.h5", "w")
S.create_dataset("/impacts/single/Sxx", data=read_signal(sys.argv[1]))
S.create_dataset("/impacts/single/Syy", data=read_signal(sys.argv[2]))
S.create_dataset("/impacts/single/Szz", data=read_signal(sys.argv[3]))
S.create_dataset("/impacts/single/Sxy", data=read_signal(sys.argv[4]))
S.create_dataset("/impacts/single/Sxz", data=read_signal(sys.argv[5]))
S.create_dataset("/impacts/single/Syz", data=read_signal(sys.argv[6]))

Z = h5py.File("Z.h5", "w")
Z.create_dataset("Z", data=read_signal(sys.argv[7]))
