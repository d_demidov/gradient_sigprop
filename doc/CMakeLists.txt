find_package(LATEX QUIET)
if (LATEX_COMPILER)
    include(UseLATEX.cmake)

    add_latex_document(signals.tex
        IMAGES field.pdf
        DEFAULT_PDF MANGLE_TARGET_NAMES
        )
endif()
