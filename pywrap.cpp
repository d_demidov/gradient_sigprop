#include <iostream>
#include <stdexcept>

#include <boost/python.hpp>
#include <boost/python/tuple.hpp>
#include <boost/python/object.hpp>
#include <boost/python/extract.hpp>
#include <boost/python/numeric.hpp>
#include <boost/python/raw_function.hpp>
#include <boost/python/stl_iterator.hpp>

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include "numpy_boost_python.hpp"

#include "sigprop.h"

//---------------------------------------------------------------------------
void precondition(bool cond, const std::string &errmsg) {
    if (!cond) throw std::logic_error(errmsg);
}

//---------------------------------------------------------------------------
boost::python::tuple sigprop_cov_6(
        const numpy_boost<double,3> &Sxx,
        const numpy_boost<double,3> &Syy,
        const numpy_boost<double,3> &Szz,
        const numpy_boost<double,3> &Sxy,
        const numpy_boost<double,3> &Sxz,
        const numpy_boost<double,3> &Syz,
        const numpy_boost<int,   3> &mask,
        const numpy_boost<double,2> &Z
        )
{
#define CHECK_SIZES(p, q)                                                      \
    precondition(                                                              \
            std::equal(p.shape(), p.shape() + 3, q.shape()), \
            "Model signals sizes mismatch (" #p " != " #q ")"                  \
            );

    CHECK_SIZES(Sxx, Syy);
    CHECK_SIZES(Sxx, Szz);
    CHECK_SIZES(Sxx, Sxy);
    CHECK_SIZES(Sxx, Sxz);
    CHECK_SIZES(Sxx, Syz);
    CHECK_SIZES(Sxx, mask);

#undef CHECK_SIZES

    int napp = Sxx.shape()[0];
    int nsen = Sxx.shape()[1];
    int lmod = Sxx.shape()[2];
    int lfld = Z.shape()[1];

    precondition(nsen == static_cast<int>(Z.shape()[0]),
            "Number of sensors mismatch in Sxx and Z");
    precondition(lfld >= lmod,
            "Signal length is not enough in field data");

    int lout = lfld - lmod + 1;
    int mdim[] = {napp, lout, 6};
    numpy_boost<double,3> M(mdim);

    int sdim[] = {napp, lout};
    numpy_boost<double,2> snr(sdim);

    int edim[] = {napp, lout, 3, 3};
    numpy_boost<double,3> eval(edim);
    numpy_boost<double,4> evec(edim);

    signal_properties_cov_6(
            M.data(), snr.data(), eval.data(), evec.data(),
            napp, nsen, lmod, lfld,
            Sxx.data(), Syy.data(), Szz.data(),
            Sxy.data(), Sxz.data(), Syz.data(),
            mask.data(), Z.data()
            );

    using namespace boost::python;

    return make_tuple(
            handle<>(borrowed(M.py_ptr())),
            handle<>(borrowed(snr.py_ptr())),
            handle<>(borrowed(eval.py_ptr())),
            handle<>(borrowed(evec.py_ptr()))
            );
}

//---------------------------------------------------------------------------
boost::python::tuple sigprop_blk(
        const numpy_boost<double,3> &Sxx,
        const numpy_boost<double,3> &Syy,
        const numpy_boost<double,3> &Szz,
        const numpy_boost<double,3> &Sxy,
        const numpy_boost<double,3> &Sxz,
        const numpy_boost<double,3> &Syz,
        const numpy_boost<int,   1> &group,
        const numpy_boost<double,2> &Z
        )
{
#define CHECK_SIZES(p, q)                                                      \
    precondition(                                                              \
            std::equal(p.shape(), p.shape() + 3, q.shape()), \
            "Model signals sizes mismatch (" #p " != " #q ")"                  \
            );

    CHECK_SIZES(Sxx, Syy);
    CHECK_SIZES(Sxx, Szz);
    CHECK_SIZES(Sxx, Sxy);
    CHECK_SIZES(Sxx, Sxz);
    CHECK_SIZES(Sxx, Syz);

#undef CHECK_SIZES

    int napp = Sxx.shape()[0];
    int nsen = Sxx.shape()[1];
    int lmod = Sxx.shape()[2];
    int lfld = Z.shape()[1];

    precondition(nsen == static_cast<int>(Z.shape()[0]),
            "Number of sensors mismatch in Sxx and Z");
    precondition(nsen == static_cast<int>(group.shape()[0]),
            "Number of sensors mismatch in Z and group");
    precondition(lfld > lmod,
            "Signal length is not enough in field data");

    int lout = lfld - lmod + 1;
    int mdim[] = {napp, lout, 6};
    numpy_boost<double,3> M(mdim);

    int sdim[] = {napp, lout};
    numpy_boost<double,2> snr(sdim);

    int edim[] = {napp, lout, 3, 3};
    numpy_boost<double,3> eval(edim);
    numpy_boost<double,4> evec(edim);

    signal_properties_blk(
            M.data(), snr.data(), eval.data(), evec.data(),
            napp, nsen, lmod, lfld,
            Sxx.data(), Syy.data(), Szz.data(),
            Sxy.data(), Sxz.data(), Syz.data(),
            group.data(), Z.data()
            );

    using namespace boost::python;

    return make_tuple(
            handle<>(borrowed(M.py_ptr())),
            handle<>(borrowed(snr.py_ptr())),
            handle<>(borrowed(eval.py_ptr())),
            handle<>(borrowed(evec.py_ptr()))
            );
}

//---------------------------------------------------------------------------
boost::python::tuple sigprop_cov_1(
        const numpy_boost<double,3> &S,
        const numpy_boost<int,   3> &mask,
        const numpy_boost<double,2> &Z
        )
{
#define CHECK_SIZES(p, q)                                                      \
    precondition(                                                              \
            std::equal(p.shape(), p.shape() + 3, q.shape()), \
            "Model signals sizes mismatch (" #p " != " #q ")"                  \
            );

    CHECK_SIZES(S, mask);

#undef CHECK_SIZES

    int napp = S.shape()[0];
    int nsen = S.shape()[1];
    int lmod = S.shape()[2];
    int lfld = Z.shape()[1];

    precondition(nsen == static_cast<int>(Z.shape()[0]),
            "Number of sensors mismatch in Sxx and Z");
    precondition(lfld >= lmod,
            "Signal length is not enough in field data");

    int lout = lfld - lmod + 1;
    int mdim[] = {napp, lout};
    numpy_boost<double,2> M(mdim);

    int adim[] = {napp};
    numpy_boost<double,1> A(adim);

    signal_properties_cov_1(
            M.data(), A.data(),
            napp, nsen, lmod, lfld,
            S.data(), mask.data(), Z.data()
            );

    using namespace boost::python;

    return make_tuple(
            handle<>(borrowed(M.py_ptr())),
            handle<>(borrowed(A.py_ptr()))
            );
}

//---------------------------------------------------------------------------
boost::python::tuple sigprop_cov_matrix(
        const numpy_boost<int,   2> &mask,
        const numpy_boost<double,2> &Z
        )
{
    int nsen = mask.shape()[0];
    int lmod = mask.shape()[1];
    int lfld = Z.shape()[1];

    precondition(nsen == static_cast<int>(Z.shape()[0]),
            "Number of sensors mismatch in Sxx and Z");
    precondition(lfld >= lmod,
            "Signal length is not enough in field data");

    int nnz = std::count_if(mask.data(), mask.data() + nsen * lmod,
            [](int m){ return m; });

    int cdim[] = {nnz, nnz};
    numpy_boost<double,2> cdir(cdim);
    numpy_boost<double,2> cinv(cdim);

    signal_properties_cov_matrix(
            cdir.data(), cinv.data(),
            nsen, lmod, lfld,
            mask.data(), Z.data()
            );

    using namespace boost::python;

    return make_tuple(
            handle<>(borrowed(cdir.py_ptr())),
            handle<>(borrowed(cinv.py_ptr()))
            );
}

//---------------------------------------------------------------------------
boost::python::tuple sigprop_var_6(
        const numpy_boost<double,3> &Sxx,
        const numpy_boost<double,3> &Syy,
        const numpy_boost<double,3> &Szz,
        const numpy_boost<double,3> &Sxy,
        const numpy_boost<double,3> &Sxz,
        const numpy_boost<double,3> &Syz,
        const numpy_boost<double,2> &Z
        )
{
#define CHECK_SIZES(p, q)                                                      \
    precondition(                                                              \
            std::equal(p.shape(), p.shape() + 3, q.shape()), \
            "Model signals sizes mismatch (" #p " != " #q ")"                  \
            );

    CHECK_SIZES(Sxx, Syy);
    CHECK_SIZES(Sxx, Szz);
    CHECK_SIZES(Sxx, Sxy);
    CHECK_SIZES(Sxx, Sxz);
    CHECK_SIZES(Sxx, Syz);

#undef CHECK_SIZES

    int napp = Sxx.shape()[0];
    int nsen = Sxx.shape()[1];
    int lmod = Sxx.shape()[2];
    int lfld = Z.shape()[1];

    precondition(nsen == static_cast<int>(Z.shape()[0]),
            "Number of sensors mismatch in Sxx and Z");
    precondition(lfld >= lmod,
            "Signal length is not enough in field data");

    int lout = lfld - lmod + 1;
    int mdim[] = {napp, lout, 6};
    numpy_boost<double,3> M(mdim);

    int sdim[] = {napp, lout};
    numpy_boost<double,2> snr(sdim);

    int edim[] = {napp, lout, 3, 3};
    numpy_boost<double,3> eval(edim);
    numpy_boost<double,4> evec(edim);

    signal_properties_var_6(
            M.data(), snr.data(), eval.data(), evec.data(),
            napp, nsen, lmod, lfld,
            Sxx.data(), Syy.data(), Szz.data(),
            Sxy.data(), Sxz.data(), Syz.data(),
            Z.data()
            );

    using namespace boost::python;

    return make_tuple(
            handle<>(borrowed(M.py_ptr())),
            handle<>(borrowed(snr.py_ptr())),
            handle<>(borrowed(eval.py_ptr())),
            handle<>(borrowed(evec.py_ptr()))
            );
}

//---------------------------------------------------------------------------
boost::python::tuple sigprop_var_1(
        const numpy_boost<double,3> &S,
        const numpy_boost<double,2> &Z
        )
{
    int napp = S.shape()[0];
    int nsen = S.shape()[1];
    int lmod = S.shape()[2];
    int lfld = Z.shape()[1];

    precondition(nsen == static_cast<int>(Z.shape()[0]),
            "Number of sensors mismatch in S and Z");
    precondition(lfld >= lmod,
            "Signal length is not enough in field data");

    int lout = lfld - lmod + 1;
    int mdim[] = {napp, lout};
    numpy_boost<double,2> M(mdim);

    int adim[] = {napp};
    numpy_boost<double,1> A(adim);

    signal_properties_var_1(
            M.data(), A.data(),
            napp, nsen, lmod, lfld,
            S.data(), Z.data()
            );

    using namespace boost::python;

    return make_tuple(
            handle<>(borrowed(M.py_ptr())),
            handle<>(borrowed(A.py_ptr()))
            );
}

#if PY_MAJOR_VERSION >= 3
void*
#else
void
#endif
call_import_array() {
    import_array();
    return NUMPY_IMPORT_ARRAY_RETVAL;
}

//---------------------------------------------------------------------------
BOOST_PYTHON_MODULE(pysigprop)
{
    using namespace boost::python;
    docstring_options docopts(true, true, false);

    call_import_array();

    numpy_boost_python_register_type<double, 2>();
    numpy_boost_python_register_type<double, 3>();
    numpy_boost_python_register_type<int,    1>();
    numpy_boost_python_register_type<int,    2>();
    numpy_boost_python_register_type<int,    3>();

    def("sigprop_cov_matrix", sigprop_cov_matrix);
    def("sigprop_cov_6",      sigprop_cov_6);
    def("sigprop_blk",        sigprop_blk);
    def("sigprop_var_6",      sigprop_var_6);
    def("sigprop_cov_1",      sigprop_cov_1);
    def("sigprop_var_1",      sigprop_var_1);
}
