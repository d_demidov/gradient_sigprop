Sxx = load('Sxx_m.txt');
Syy = load('Syy_m.txt');
Szz = load('Szz_m.txt');
Sxy = load('Sxy_m.txt');
Sxz = load('Sxz_m.txt');
Syz = load('Syz_m.txt');
Z45 = load('Z45_m.txt');

sensors = reshape(1:49, [7,7]);
sensors = sensors(1:3:end,1:3:end);
sensors = sensors(:);

Sxx = Sxx(sensors, 3:3:end);
Syy = Syy(sensors, 3:3:end);
Szz = Szz(sensors, 3:3:end);
Sxy = Sxy(sensors, 3:3:end);
Sxz = Sxz(sensors, 3:3:end);
Syz = Syz(sensors, 3:3:end);
Z45 = Z45(sensors, 3:3:end);

Sxx = Sxx(:, 50:200);
Syy = Syy(:, 50:200);
Szz = Szz(:, 50:200);
Sxy = Sxy(:, 50:200);
Sxz = Sxz(:, 50:200);
Syz = Syz(:, 50:200);

[nsen, lmod] = size(Sxx);

Sxx = reshape(Sxx, [1, nsen, lmod]);
Syy = reshape(Syy, [1, nsen, lmod]);
Szz = reshape(Szz, [1, nsen, lmod]);
Sxy = reshape(Sxy, [1, nsen, lmod]);
Sxz = reshape(Sxz, [1, nsen, lmod]);
Syz = reshape(Syz, [1, nsen, lmod]);

theta = 1e-16;
Mask = (                 ...
    (abs(Sxx) > theta) + ...
    (abs(Syy) > theta) + ...
    (abs(Szz) > theta) + ...
    (abs(Sxy) > theta) + ...
    (abs(Sxz) > theta) + ...
    (abs(Syz) > theta)   ...
    ) > 0;

O = zeros(nsen, 5000);
Z45 = [O Z45 O];
Z45 = Z45 + 1e-16 * randn(size(Z45));

G = 0:nsen-1
[M, SNR, W, V] = sigprop_blk(Sxx, Syy, Szz, Sxy, Sxz, Syz, G, Z45);
%[M, SNR, W, V] = sigprop_cov_6(Sxx, Syy, Szz, Sxy, Sxz, Syz, Mask, Z45);
%[M, SNR, W, V] = sigprop_var_6(Sxx, Syy, Szz, Sxy, Sxz, Syz, Z45);
%[M, A] = sigprop_cov_1(Sxx, Mask, Z45);
%[M, A] = sigprop_var_1(Sxx, Z45);
