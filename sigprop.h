#ifndef SIGPROP_H
#define SIGPROP_H

#ifdef __cplusplus
extern "C" {
#endif

void signal_properties_cov_matrix(
        double *cov_dir,    // [sum(mask) x sum(mask)]
        double *cov_inv,    // [sum(mask) x sum(mask)]
        int nsen,
        int lmod,
        int lfld,
        const int    *mask, // [nsen x lmod]
        const double *z     // [nsen x lfld]
        );

/// Восстановление параметров ударов по модельным и полевым сигналам.
/**
 * \param[in]  napp Число точек приложения модельных ударов.
 * \param[in]  nsen Число датчиков на поверхности.
 * \param[in]  lmod Число отсчетов в модельных сигналах.
 * \param[in]  lfld Число отсчетов в полевом сигнале.
 * \param[in]  Sxx  Модельный сигнал     [napp x nsen x lmod]
 * \param[in]  Syy  Модельный сигнал     [napp x nsen x lmod]
 * \param[in]  Szz  Модельный сигнал     [napp x nsen x lmod]
 * \param[in]  Sxy  Модельный сигнал     [napp x nsen x lmod]
 * \param[in]  Sxz  Модельный сигнал     [napp x nsen x lmod]
 * \param[in]  Syz  Модельный сигнал     [napp x nsen x lmod]
 * \param[in]  Z    Полевой сигнал       [nsen x lfld]
 * \param[out] M    Элементы матрицы M   [napp x lfld x 6]
 * \param[out] eval Собственные значения [napp x lfld x 3]
 * \param[out] evec Собственные векторы  [napp x lfld x 3 x 3]
 */
void signal_properties_cov_6(
        double *M,
        double *snr,
        double *eval,
        double *evec,
        int napp,
        int nsen,
        int lmod,
        int lfld,
        const double *Sxx,
        const double *Syy,
        const double *Szz,
        const double *Sxy,
        const double *Sxz,
        const double *Syz,
        const int    *mask,
        const double *Z
        );

void signal_properties_blk(
        double *M,
        double *snr,
        double *eval,
        double *evec,
        int napp,
        int nsen,
        int lmod,
        int lfld,
        const double *Sxx,
        const double *Syy,
        const double *Szz,
        const double *Sxy,
        const double *Sxz,
        const double *Syz,
        const int    *group,
        const double *Z
        );

void signal_properties_var_6(
        double *M,
        double *snr,
        double *eval,
        double *evec,
        int napp,
        int nsen,
        int lmod,
        int lfld,
        const double *Sxx,
        const double *Syy,
        const double *Szz,
        const double *Sxy,
        const double *Sxz,
        const double *Syz,
        const double *Z
        );

void signal_properties_cov_1(
        double *A,
        double *snr,
        int napp,
        int nsen,
        int lmod,
        int lfld,
        const double *S,
        const int    *mask,
        const double *Z
        );

void signal_properties_var_1(
        double *A,
        double *snr,
        int napp,
        int nsen,
        int lmod,
        int lfld,
        const double *S,
        const double *Z
        );

#ifdef __cplusplus
} // extern "C"
#endif

#endif
